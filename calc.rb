class Calc
  
  attr_accessor :input

  def initialize
    @input = []
  end

  def get(*numbers)
    @input << numbers
  end

  def plus
    result = @input.flatten.inject { |sum,x| sum + x }
    @input.clear
    result
  end

  def minus
    result = 0 - @input.flatten.inject { |sum,x| sum + x }
    @input.clear
    result
  end
end
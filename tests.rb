require "rspec"
require "./calc.rb"

describe Calc do
  before(:each) do
    @calc = Calc.new
  end

  it "Method 'plus': summing single values" do
    @calc.get(10)
    @calc.get(20)
    @calc.plus.should == 30
  end

  it "Method 'plus': summing an array's members" do
    @calc.get([5, 7, 9, 11])
    @calc.plus.should == 32
  end

  it "Method 'plus': summing an array's numbers and a single number" do
    @calc.get(8)
    @calc.get([5, 7])
    @calc.plus.should == 20
  end

  it "Method 'minus': zero minus the result of the method 'plus'" do
    @calc.get(8)
    @calc.get([5, 7])
    @calc.minus.should == -20
  end

  it "Whenever a non-numeric symbol is entered, it would return nil" do
    pending "It is not implemented yet"
  end
  
end